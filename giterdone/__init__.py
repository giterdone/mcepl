# Copyright 2009 -- Robert Toscano

import os, tempfile
import glib, gtk, gobject, gedit
import git, repositorymanager as rm

emblem_names = { 'untracked': 'emblem-new',
                 'dirty': 'emblem-danger',
                 'staged': 'emblem-draft',
                 'synced': 'emblem-default' }


class Giterdone(gedit.Plugin):
    commit_msg_header = '''

# To cancel this commit close this file without making a change. To commit,
# write your message, then save and close the file.
#
'''

    def activate(self, window):
        print 'activated'

        self.window = window

        mb = window.get_message_bus()
        self.bus_ids = []
        self.bus_ids.append( mb.connect('/plugins/filebrowser', 'inserted', self.fb_inserted) )
        self.bus_ids.append( mb.connect('/plugins/filebrowser', 'deleted', self.fb_deleted) )
        self.bus_ids.append( mb.connect('/plugins/filebrowser', 'root_changed', self.fb_root_changed) )

        major, minor, rev = gedit.version
        
        # get the filebrowser view
        if (major > 2) or (major == 2 and minor > 27) or (major == 2 and minor == 27 and rev >= 5):
            msg = mb.send_sync('/plugins/filebrowser', 'get_view')
            fb_view = msg.get_value('view')

        else:
            for child in self.window.get_side_panel().get_children():
                if isinstance(child, gtk.Notebook):
                    for page in child.get_children():
                        if page.get_name() == 'GeditFileBrowserWidget':
                            fb_view = page.get_children()[2].get_children()[0]
                            break
                    break

        self.fb_action_ids = {}
        self.fb_context_ids = []

        print('ids', major, minor, rev)

        if (major > 2) or (major == 2 and minor > 27) or (major == 2 and minor == 27 and rev >= 4):
            # !! As of gedit version 2.28.0 (and probably before) this call will cause a warning
            # if the plugin is activated, deactivated, and reactivated in one gedit session because
            # the FB does not properly remove the context item via the message 'remove_context_item'
            # This results in having to reboot gedit if you want to reactivate (which doesn't happen
            # much anyway).
            # TODO: Will submit fix to gedit team.
        
            # git-add context-menu item
            add_action = gtk.Action('git-add', 'git add',
                                    'Add file to staging area', gtk.STOCK_ADD)
            self.fb_action_ids[add_action] = add_action.connect('activate',
                                                                self.on_git_add,
                                                                fb_view)
            msg = mb.send_sync('/plugins/filebrowser', 'add_context_item',
                               action=add_action, path='/FilePopup/FilePopup_Opt3')
            self.fb_context_ids.append( msg.get_value('id') )

            # git-reset context-menu item
            reset_action = gtk.Action('git-reset', 'git reset',
                                    'Reset file from staging area', gtk.STOCK_REMOVE)
            self.fb_action_ids[reset_action] = reset_action.connect('activate',
                                                                self.on_git_reset,
                                                                fb_view)
            msg = mb.send_sync('/plugins/filebrowser', 'add_context_item',
                               action=reset_action, path='/FilePopup/FilePopup_Opt3')
            self.fb_context_ids.append( msg.get_value('id') )

            # git-commit context-menu item
            commit_action = gtk.Action('git-commit','git commit',
                                       'Commit staged files', gtk.STOCK_SAVE_AS)
            self.fb_action_ids[commit_action] = commit_action.connect('activate',
                                                                     self.on_git_commit,
                                                                     fb_view)
            msg = mb.send_sync('/plugins/filebrowser', 'add_context_item',
                               action=commit_action, path='/FilePopup/FilePopup_Opt3')
            self.fb_context_ids.append( msg.get_value('id') )

            # git-diff context-menu item
            diff_action = gtk.Action('git-diff','git diff',
                                     'Diff file', gtk.STOCK_COPY)
            self.fb_action_ids[diff_action] = diff_action.connect('activate',
                                                                  self.on_git_diff,
                                                                  fb_view)
            msg = mb.send_sync('/plugins/filebrowser', 'add_context_item',
                               action=diff_action, path='/FilePopup/FilePopup_Opt3')
            self.fb_context_ids.append( msg.get_value('id') )

            fb_view.get_selection().connect('changed', self.on_fb_selection_changed)

        self.commit_tabs = {}
        self.diff_tabs = []

        # listen for tab closes (these might be git commit messages or git diffs)
        self.on_tab_closed_id = self.window.connect('tab-removed', self.on_tab_closed)

        # generate a status-bar context id
        self.status_id = self.window.get_statusbar().get_context_id('giterdone')

        self.repos = {}
        self.ids = {}
        self.repo_hashes = {}
        self.deactivating = False

        # Create a "Repository Manager" page
        self.rm = rm.RepositoryManager(self.repos, self.window, self.status_id, self.commit_msg_header)
        self.window.get_side_panel().add_item(self.rm, 'Repository Manager', gtk.STOCK_NETWORK)

        # Cause a fb_root_changed message so we can get all those fb_inserted calls
        msg = mb.send_sync( '/plugins/filebrowser', 'get_root' )
        if msg.get_value('uri'):
            # HACK: We have to first set the root to something else (in this case, I chose root) or
            # else when we reset the root, nothing will change because the filebrowser knows that we
            # haven't changed the root
            mb.send_sync( '/plugins/filebrowser', 'set_root', uri='file:///' )
            mb.send_sync( '/plugins/filebrowser', 'set_root', uri=msg.get_value('uri') )

        # check repos for changes every second, but only do it when idle
        glib.timeout_add(1000, self.check_repos, priority=glib.PRIORITY_DEFAULT_IDLE)
        

    def deactivate(self, window):
        print 'deactivated'
        self.deactivating = True
        for action, id in self.fb_action_ids.iteritems(): action.disconnect(id)
        self.window.disconnect(self.on_tab_closed_id)
        bus = self.window.get_message_bus()
        for bus_id in self.bus_ids: bus.disconnect(bus_id)
        
        self.window.get_side_panel().remove_item(self.rm)
        # if true, then we're just deactivating the plugin, if false, we're shutting down the whole 
        # program. In the latter case, there is no FileBrowser to talk to
        if bus.is_registered( '/plugins/filebrowser', 'get_root' ):
            msg = bus.send_sync( '/plugins/filebrowser', 'get_root' )
            bus.send_sync( '/plugins/filebrowser', 'set_root', uri='file:///' )
            bus.send_sync( '/plugins/filebrowser', 'set_root', uri=msg.get_value('uri') )
            
            # remove context_items from FB
            for id in self.fb_context_ids:
                bus.send_sync( '/plugins/filebrowser', 'remove_context_item', id=id )


    def update_ui(self, window):
        pass


    def fb_inserted(self, bus, message):
        print 'inserted'

        if not self.is_root_local: return

        id = message.get_value('id')
        uri = message.get_value('uri')
        is_dir = message.get_value('is_directory')

        # might be cheaper to instantiate a MessageType and then send that message to everyone
        #bus.send('/plugins/filebrowser', 'set_emblem', id=id, emblem='emblem-default')

        path = uri[7:]

        git_dir = git.find_repo(path)
        if not git_dir: return

        self.ids[path] = id

        if is_dir and not git_dir in self.repos.keys():
            self.repos[git_dir] = git.status(git_dir)
            self.rm.add_repo(git_dir)

        repo = self.repos[git_dir]

        if path in repo['ignored']:
            return
        elif path in repo['untracked']:
            bus.send('/plugins/filebrowser', 'set_emblem', id=id, emblem=emblem_names['untracked'])
        elif path in repo['dirty']:
            bus.send('/plugins/filebrowser', 'set_emblem', id=id, emblem=emblem_names['dirty'])
        elif path in repo['staged']:
            bus.send('/plugins/filebrowser', 'set_emblem', id=id, emblem=emblem_names['staged'])
        else:
            bus.send('/plugins/filebrowser', 'set_emblem', id=id, emblem=emblem_names['synced'])


    def fb_deleted(self, bus, message):
        print 'deleted'


    def fb_root_changed(self, bus, message):
        print 'root_changed'
        self.is_root_local = message.get_value('uri').startswith('file://')
        self.curr_root = message.get_value('uri')[len('file://'):]

        # refresh the manager
        self.rm.update_root(self.curr_root)

        git_dir = git.find_repo(self.curr_root)
        if not git_dir: return
        if not git_dir in self.repos.keys():
            self.repos[git_dir] = git.status(git_dir)
            self.rm.add_repo(git_dir)


    def on_git_add(self, action, view):
        model, rows = view.get_selection().get_selected_rows()

        paths = [ model[row][2][len('file://'):] for row in rows ]

        git.add(paths)

        # flash statusbar message
        msg = 'Added '+str( len(paths) )+' files'
        self.window.get_statusbar().flash_message(self.status_id, msg)


    def on_git_reset(self, action, view):
        model, rows = view.get_selection().get_selected_rows()

        paths = [ model[row][2][len('file://'):] for row in rows ]

        git.reset(paths)

        # flash statusbar message
        msg = 'Reset '+str( len(paths) )+' files'
        self.window.get_statusbar().flash_message(self.status_id, msg)


    def on_git_commit(self, action, view):
        print 'on git commit'
        model, rows = view.get_selection().get_selected_rows()

        paths = [ model[row][2][len('file://'):] for row in rows ]

        git_dir = git.find_repo(paths[0])

        f = tempfile.NamedTemporaryFile( suffix='.commit', prefix='gedit-git-commit-message-',
                                         delete=False )
        f.write( self.commit_msg_header + git.status_msg(git_dir, paths) )
        commit_msg_path = f.name
        f.close()

        # open file in gedit
        tab = self.window.create_tab_from_uri('file://'+commit_msg_path,
                                              gedit.encoding_get_utf8(), 0, False, True)
        tab.get_document().goto_line(0)
        self.commit_tabs[tab] = paths


    def on_tab_closed(self, window, tab):

        if tab in self.commit_tabs.keys():
            commit_msg_path = tab.get_document().get_uri()[len('file://'):]

            # check if commit_msg had any text in it
            has_msg = False
            f = open(commit_msg_path, 'r')
            for line in f:
                line = line.strip(' \n')
                if line.startswith('#'):
                    continue
                elif line != '':
                    print 'Line has message: \''+line+'\''
                    has_msg = True
                    break
            f.close()

            if has_msg:
                git.commit_files(self.commit_tabs[tab], commit_msg_path)

            # clean up
            del self.commit_tabs[tab]
            os.remove(commit_msg_path)

        elif tab in self.diff_tabs:
            self.diff_tabs.remove(tab)
            os.remove( tab.get_document().get_uri()[len('file://'):] )


    def on_repo_changed(self, git_dir):
        self.repos[git_dir] = git.status(git_dir)
        bus = self.window.get_message_bus()
        bus.send('/plugins/filebrowser', 'refresh')
        self.rm.update_repo(git_dir)


    def check_repos(self):
        for git_dir in self.repos.keys():
            if not git_dir in self.repo_hashes.keys():
                # calculate hash
                self.repo_hashes[git_dir] = hash( git.status_msg(git_dir) )

            else:
                # check if status message changed
                curr_hash = hash( git.status_msg(git_dir) )

                if curr_hash != self.repo_hashes[git_dir]:
                    self.repo_hashes[git_dir] = curr_hash
                    self.on_repo_changed(git_dir)

        return not self.deactivating


    def on_git_diff(self, action, view):
        model, rows = view.get_selection().get_selected_rows()

        paths = [ model[row][2][len('file://'):] for row in rows ]

        diff = git.diff(paths)

        f = tempfile.NamedTemporaryFile(suffix='.diff', prefix='gedit-git-diff-', delete=False)
        f.write(diff)
        diff_path = f.name
        f.close()

        # open file in gedit
        tab = self.window.create_tab_from_uri('file://'+diff_path,
                                              gedit.encoding_get_utf8(), 0, False, True)
        tab.get_document().goto_line(0)
        self.diff_tabs.append(tab)


    def on_fb_selection_changed(self, selection):
        model, rows = selection.get_selected_rows()

        if len(rows) == 0:
            for action in self.fb_action_ids.keys():
                action.set_sensitive(False)
            return

        paths = [ model[row][2][len('file://'):] for row in rows ]

        # check if the first path is under a repository
        for repopath in self.repos.keys():
            if paths[0].startswith(repopath):
                under_repo = True
                git_dir = repopath
                break
        else:
            under_repo = False

        if not under_repo:
            for action in self.fb_action_ids.keys():
                action.set_sensitive(False)
            return

        # check if all selected paths are in the same repo
        for path in paths:
            if not path.startswith(git_dir):
                for action in self.fb_action_ids.keys():
                    action.set_sensitive(False)
                return

        for action in self.fb_action_ids.keys():
            action.set_sensitive(True)
