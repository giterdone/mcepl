# Copyright 2009 -- Robert Toscano

import os.path, tempfile
import gtk
import gedit
import git

popup_xml = '''<ui>
  <popup name="RepositoryManagerPopup">
    <menuitem action="GitCommit"/>
    <menuitem action="GitPull"/>
    <menuitem action="GitFetch"/>
    <menuitem action="GitPush"/>
  </popup>
</ui>'''

def load_emblem(name):
    return gtk.icon_theme_get_default().load_icon(name, 20, gtk.ICON_LOOKUP_USE_BUILTIN)


emblems = { 'synced':    load_emblem('emblem-default'),
            'untracked': load_emblem('emblem-new'),
            'dirty':  load_emblem('emblem-danger'),
            'staged':    load_emblem('emblem-draft') }


class RepositoryManager(gtk.TreeView):
    def __init__(self, repos, window, statusbar_id, commit_msg_header):
        self.iters = {}
        self.repos = repos
        self.gedit_window = window
        self.statusbar = window.get_statusbar()
        self.statusbar_id = statusbar_id
        self.commit_msg_header = commit_msg_header

        self.model = gtk.ListStore(str, # full path to repo
                                   str, # name to be displayed
                                   str, # branch
                                   gtk.gdk.Pixbuf) # emblem

        super(RepositoryManager, self).__init__(self.model)

        self.set_grid_lines(False)
        self.set_headers_clickable(True)
        self.append_column(gtk.TreeViewColumn(None, gtk.CellRendererPixbuf(), pixbuf=3))
        self.append_column(gtk.TreeViewColumn('Name', gtk.CellRendererText(), text=1))
        self.append_column(gtk.TreeViewColumn('Branch', gtk.CellRendererText(), text=2))
        self.connect('button-release-event', self.on_button_released)

        # Create the Repository Manager popup
        self.uimanager = gtk.UIManager()
        self.uimanager.add_ui_from_string(popup_xml)

        self.actiongroup = gtk.ActionGroup('GitBase')
        self.actiongroup.add_actions([ ('GitCommit', gtk.STOCK_SAVE_AS, 'Commit', None,
                                            'Commit all staged files', self.on_git_commit_all),
                                       ('GitPush', gtk.STOCK_GOTO_TOP, 'Push', None,
                                            'Push to origin repository', self.on_git_push),
                                       ('GitPull', gtk.STOCK_GOTO_BOTTOM, 'Pull', None,
                                            'Pull from remote reposities', self.on_git_pull),
                                       ('GitFetch', gtk.STOCK_GO_DOWN, 'Fetch', None,
                                            'Fetch from remote repositories', self.on_git_fetch) ])

        self.uimanager.insert_action_group(self.actiongroup, 0)

        self.commit_tabs = {}
        self.on_tab_closed_id = window.connect('tab-removed', self.on_tab_closed)

        # watch for selection changes and make the actions sensitive as needed
        self.get_selection().connect('changed', self.on_selection_changed)


    def update_root(self, root):
        '''Adds/removes repos to/from the RM based on the new root and each repo's path'''

        self.curr_root = root
        repopaths = self.repos.keys()
        toadd = []
        for path in repopaths:
            if path.startswith(self.curr_root): toadd.append(path)

        toremove = []
        tokeep = []
        for row in self.model:
            if not row[0].startswith(self.curr_root): toremove.append(row)
            else: tokeep.append(row[0])

        # remove paths already in the model from toadd
        for path in tokeep: toadd.remove(path)

        # remove paths from the model
        for row in toremove:
            self.model.remove(row.iter)

        # modify existing rows to match new root
        for row in self.model:
            if row[0] == self.curr_root: row[1] = os.path.basename(row[0])
            elif self.curr_root.startswith(row[0]): row[1] = os.path.basename(row[0])
            else: row[1] = '.'+row[0][len(self.curr_root):]

        # append new repos
        for path in toadd:
            self.add_repo(path)


    def add_repo(self, repopath):
        branch = 'master'
        if repopath == self.curr_root:
            name = os.path.basename(repopath)
        else:
            name = '.'+repopath[len(self.curr_root):]

        status = self.repos[repopath]

        if repopath in status['untracked']: emblem = emblems['untracked']
        elif repopath in status['dirty']:   emblem = emblems['dirty']
        elif repopath in status['staged']:  emblem = emblems['staged']
        else:                               emblem = emblems['synced']

        self.iters[repopath] = self.model.append((repopath, name, branch, emblem))


    def update_repo(self, repopath):
        print 'updating repo: '+repopath
        status = self.repos[repopath]

        if repopath in status['untracked']: emblem = emblems['untracked']
        elif repopath in status['dirty']:   emblem = emblems['dirty']
        elif repopath in status['staged']:  emblem = emblems['staged']
        else:                               emblem = emblems['synced']

        self.model.set_value(self.iters[repopath], 3, emblem)


    def on_button_released(self, widget, event):
        if event.button == 3:
            self.uimanager.get_widget('/RepositoryManagerPopup').popup(None, None, None, event.button, 0)


    def on_git_commit_all(self, action):
        model, rows = self.get_selection().get_selected_rows()

        if len(rows) > 1:
            gtk.MessageDialog().run()
            return

        git_dir = model[rows[0]][0]

        f = tempfile.NamedTemporaryFile(suffix='.commit', prefix='gedit-git-commit-message-', delete=False)
        f.write( self.commit_msg_header + git.status_msg(git_dir) )
        msg_path = f.name
        f.close()

        # open file in gedit
        tab = self.gedit_window.create_tab_from_uri('file://'+msg_path,
                                              gedit.encoding_get_utf8(), 0, False, True)
        tab.get_document().goto_line(0)
        self.commit_tabs[tab] = git_dir

    def on_git_pull(self, action):
        model, rows = self.get_selection().get_selected_rows()

        for path in [ row[0] for row in rows ]:
            pass

    def on_git_push(self, action):
        model, rows = self.get_selection().get_selected_rows()

        for path in [ row[0] for row in rows ]:
            pass

    def on_git_fetch(self, action):
        model, rows = self.get_selection().get_selected_rows()

        for path in [ model[row][0] for row in rows ]:
            self.statusbar.flash_message(self.statusbar_id, 'Fetching repository: '+path)
            git.fetch(path)


    def on_tab_closed(self, window, tab):
        if tab in self.commit_tabs.keys():
            git_dir = self.commit_tabs[tab]
            commit_msg_path = tab.get_document().get_uri()[len('file://'):]

            # check if commit_msg had any text in it
            has_msg = False
            f = open(commit_msg_path, 'r')
            for line in f:
                line = line.strip(' \n')
                if line.startswith('#'):
                    continue
                elif line != '':
                    print 'Line has message: \''+line+'\''
                    has_msg = True
                    break
            f.close()

            if has_msg:
                git.commit(git_dir, commit_msg_path)

            # clean up
            del self.commit_tabs[tab]
            os.remove(commit_msg_path)


    def on_selection_changed(self, selection):
        model, rows = selection.get_selected_rows()

        if len(rows) != 1:
            for action in self.actiongroup.list_actions():
                action.set_sensitive(False)
            return

        selected_path = model[rows[0]][0]

        if len(self.repos[selected_path]['staged']) == 0:
            for action in self.actiongroup.list_actions():
                action.set_sensitive(True)
            self.actiongroup.get_action('GitCommit').set_sensitive(False)

        else:
            for action in self.actiongroup.list_actions():
                action.set_sensitive(True)
