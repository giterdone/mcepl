# Copyright 2009 -- Robert Toscano

import os, os.path, subprocess

def status(path):
    if not is_under_git(path):
        print 'Trying to find status of non-git path: '+path
        return { 'ignored': None,
                 'untracked': None,
                 'dirty': None,
                 'staged': None }

    untracked = []
    ignored = []
    dirty = []
    staged = []

    # find untracked files
    proc = subprocess.Popen(['git', 'clean', '--dry-run', '-d'],
                            cwd=path,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        if not line.startswith('Would remove '):
            print 'Unexpected line in "'+' '.join(['git', 'clean', '--dry-run', '-d'])+'": '+line
            return { 'ignored': None,
                     'untracked': None,
                     'dirty': None,
                     'staged': None }

        name = line[len('Would remove '):].strip('/')
        abs_name = os.path.join(path,name)
        untracked.append( abs_name )

        # recursively add parent directories
        cur_dir = os.path.dirname(abs_name)
        while cur_dir != path:
            if cur_dir in untracked: break
            untracked.append(cur_dir)
            cur_dir = os.path.dirname(cur_dir)

        if os.path.isdir( abs_name ):
            # add all sub files
            for dirpath, dirnames, filenames in os.walk( abs_name ):
                for f in dirnames+filenames:
                    untracked.append( os.path.join(dirpath,f) )

    if untracked: untracked.append(path)

    # find ignored files
    proc = subprocess.Popen(['git', 'clean', '--dry-run', '-Xd'],
                            cwd=path,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        if not line.startswith('Would remove '):
            print 'Unexpected line in "'+' '.join(['git', 'clean', '--dry-run', '-Xd'])+'": '+line
            return { 'ignored': None,
                     'untracked': None,
                     'dirty': None,
                     'staged': None }

        name = line[len('Would remove '):].strip('/')

        ignored.append( os.path.join(path, name) )


    # find dirty files
    proc = subprocess.Popen(['git', 'diff', '--name-only'],
                            cwd=path,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        name = line
        abs_name = os.path.join(path, name)
        dirty.append( abs_name )

        # recursively add parent directories
        cur_dir = os.path.dirname(abs_name)
        while cur_dir != path:
            if cur_dir in dirty: break
            dirty.append(cur_dir)
            cur_dir = os.path.dirname(cur_dir)

    if dirty: dirty.append(path)

    # find staged files
    proc = subprocess.Popen(['git', 'diff', '--cached', '--name-only'],
                            cwd=path,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        name = line
        abs_name = os.path.join(path, name)
        staged.append( abs_name )

        # recursively add parent directories
        cur_dir = os.path.dirname(abs_name)
        while cur_dir != path:
            if cur_dir in dirty: break
            staged.append(cur_dir)
            cur_dir = os.path.dirname(cur_dir)

    if staged: staged.append(path)

    return { 'untracked': untracked,
             'ignored': ignored,
             'dirty': dirty,
             'staged': staged }


def add(files):
    git_dir = find_repo(files[0])
    proc = subprocess.Popen(['git', 'add', '--'] + files, cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()


def reset(files):
    git_dir = find_repo(files[0])
    proc = subprocess.Popen(['git', 'reset', '--'] + files, cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()


def status_msg(git_dir, files=None):
    if files == None:
        proc = subprocess.Popen(['git', 'status'], cwd=git_dir, stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        return stdout

    else:
        proc = subprocess.Popen(['git', 'status', '--'] + files, cwd=git_dir, stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        return stdout


def commit(git_dir, commit_msg_path):
    proc = subprocess.Popen(['git', 'commit', '--file='+commit_msg_path], cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    # TODO handle any error cases


def commit_files(files, commit_msg_path):
    git_dir = find_repo(files[0])

    proc = subprocess.Popen(['git', 'commit', '--file='+commit_msg_path, '--'] + files, cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    # TODO handle any error cases


def fetch(git_dir):
    proc = subprocess.Popen(['git', 'fetch'], cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    # TODO handle any error cases


def is_dirty(file_name):
    if os.path.isdir(file_name):
        git_dir = os.path.dirname(file_name)
        proc = subprocess.Popen(['git', 'diff', file_name], cwd=git_dir, stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        return stdout != ''

    else:
        git_dir = os.path.dirname(file_name)
        proc = subprocess.Popen(['git', 'diff', '--quiet', file_name], cwd=git_dir, stdout=subprocess.PIPE)
        proc.wait()
        return proc.returncode != 0

def is_under_git(file_name):
    # iteratively check up the directories for a .git folder
    cur_dir = file_name
    while cur_dir != '/':
        if os.path.isdir(os.path.join(cur_dir,'.git')): return True
        cur_dir = os.path.sep.join(os.path.split(cur_dir)[:-1])

    return False

def is_ignored(file_name):
    git_dir = os.path.dirname(file_name)

    proc = subprocess.Popen(['git', 'clean', '--dry-run', '-Xd'],
                            cwd=git_dir,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        if not line.startswith('Would remove '):
            print 'Unexpected line in "'+' '.join(['git', 'clean', '--dry-run', '-Xd'])+'": '+line
            return False

        name = line[len('Would remove '):]
        name = name.strip('/')

        if os.path.join(git_dir, name) == file_name: return True

    return False


def is_untracked(file_name):
    git_dir = os.path.dirname(file_name)

    proc = subprocess.Popen(['git', 'clean', '--dry-run', '-d'],
                            cwd=git_dir,
                            stdout=subprocess.PIPE)

    stdout, stderr = proc.communicate()

    for line in stdout.split('\n'):
        if not line: continue
        if not line.startswith('Would remove '):
            print 'Unexpected line in "'+' '.join(['git', 'clean', '--dry-run', '-d'])+'": '+line
            return False

        name = line[len('Would remove '):]
        name = name.strip('/')

        if os.path.isdir(file_name):
            if os.path.join(git_dir, name).startswith(file_name): return True

        else:
            if os.path.join(git_dir, name) == file_name: return True

    return False

def find_repo(path):
    # iteratively check up the directories for a .git folder
    cur_dir = path
    while cur_dir != '/':
        if os.path.isdir(os.path.join(cur_dir,'.git')): return cur_dir
        cur_dir = os.path.sep.join(os.path.split(cur_dir)[:-1])

    return None

def diff(paths):
    '''Assumes that all paths are in the same git repository'''

    git_dir = find_repo(paths[0])

    if not git_dir: raise RuntimeError('File not in a git repository')

    p = subprocess.Popen(['git', 'diff'] + paths, cwd=git_dir, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()

    return stdout
